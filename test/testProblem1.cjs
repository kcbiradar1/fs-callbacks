const problem1 = require('../problem1.cjs');

const fs = require('fs')

const createFileCallback = (file_name , deleteFileCallback) => {
    fs.writeFile(file_name , `"hi"` , "utf-8" , (error) => {
        if(error) {
            console.log(error);
        } else {
            console.log("The random file is created successfully.");
            deleteFileCallback(file_name);
        }
    })
}

const deleteFileCallback = (file_name) => {
    fs.unlink(file_name , (error) => {
        if(error) {
            console.log("Error occured while deleting file");
        } else {
            console.log("File deleted successfully.")
        }
    })
}

problem1("random-directory" , createFileCallback , deleteFileCallback);
