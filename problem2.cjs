const fs = require("fs");

function perform_actions(path) {
  fs.readFile(path, "utf-8", (error, data) => {
    if (error) {
      console.log("Error occured while reading file data");
    } else {
      const new_data = data.toUpperCase();
      const file_name = "upperCaseData.txt"; 

      fs.writeFile(file_name,new_data,'utf-8' , (error) => {
        if(error) {
          console.error("Error occured while creating data file");
        } else {
          console.log("UpperCaseData file is created successfully!!!");
        }
      });

      fs.writeFile("filenames.txt",file_name,'utf-8' , (error) => {
        if(error) {
          console.error("Error occured while creating file");
        }
      });

      fs.readFile(file_name , 'utf-8' , (error , data) => {
        if(error) {
          console.log("Error occured while reading the file!!!");
        } else {
          const get_data = data.toLowerCase();
          let splited_data = get_data.split('.');
          
          const spilted_data_file_name = "spilted_data.txt";
          
          fs.writeFile(spilted_data_file_name , JSON.stringify(splited_data) , 'utf-8', (error) => {
            if(error) {
              console.log("Error occured while creating file");
            } else {
              console.log("Splitted data is created successfully!!!");
            }
          });

          fs.appendFile('filenames.txt' , `\n${spilted_data_file_name}` , (err) => {
            if(err) {
              console.log("Error occured while appending data!!!");
            }
          });

          let sort_data = splited_data.sort();
          
          const sorted_file_name = "sorted_data.txt";
          fs.writeFile(sorted_file_name , JSON.stringify(sort_data), 'utf-8' , (error) => {
            if(error) {
              console.log("error occured while creating file");
            } else {
              console.log("Sorted data is created successfully!!!");
            }
          });

          fs.appendFile('filenames.txt' , `\n${sorted_file_name}` , (err) => {
            if(err) {
              console.log("Error occured while appending data!!!");
            }
          })

          fs.readFile('filenames.txt', 'utf-8' , (error , data) => {
            if(error) {
              console.log("Error occured while creating file");
            } else {
              let store_file_names = data.split('\n');
              for(let file_name of store_file_names) {
                fs.unlink(file_name , (error) => {
                  if(error) {
                    console.error("error occured while creating file");
                  } else {
                    console.log(`${file_name} is deleted successfully`);
                  }
                })
              }
            }
          })

        }
      })
    }
  });
}
module.exports = perform_actions;
