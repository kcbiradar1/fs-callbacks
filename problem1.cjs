const fs = require('fs');

function createDirectory(directory_name , createFileCallback , deleteFileCallback) {
    try {
        fs.mkdir(`${directory_name}` , (error) => {

            if(error) {
                console.log(error);
            } else {
                console.log("The random folder is created successfully.");

                for(let random_file_number = 1; random_file_number < 5; random_file_number++) {
                    const file_name = `${directory_name}/random_file_${random_file_number}.json`;
                    createFileCallback(file_name , deleteFileCallback);
                }
            }
        });
    } catch(error) {
        console.error("Error occured while creating directory.");
    }
}

module.exports = createDirectory;